package ru.nlmk.study.serialization.var1;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Reader {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("person.out"));
        Person igorRestored = (Person) objectInputStream.readObject();
        Person petrRestored = (Person) objectInputStream.readObject();

        System.out.println("Igor from file = " + igorRestored);
        System.out.println("Petr from file = " + petrRestored);
    }
}
