package ru.nlmk.study.serialization.var1;

import java.io.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Person igor = new Person("Igor", "Petrov", 20, new Home("Planernaya, 54-12"),
                Arrays.asList(2, 4, 6));
        Person petr = new Person("Petr", "Ivanov", 22, new Home("Planernaya, 54-14"),
                Arrays.asList(7, 6, 3));

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("person.out"));
        objectOutputStream.writeObject(igor);
        objectOutputStream.writeObject(petr);
        objectOutputStream.close();

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("person.out"));
        Person igorRestored = (Person) objectInputStream.readObject();
        Person petrRestored = (Person) objectInputStream.readObject();

        System.out.println("Igor from file = " + igorRestored);
        System.out.println("Petr from file = " + petrRestored);
    }
}
