package ru.nlmk.study.serialization.var1;

import java.io.Serializable;
import java.util.List;

public class Person implements Serializable {
    private static final long serialVersionUID = 2L;

    private String name;
    private String familyName;
    private int age;
    private Home home;
    private List<Integer> numbers;
    private String someField;

    public Person(String name, String familyName, int age, Home home, List<Integer> numbers) {
        this.name = name;
        this.familyName = familyName;
        this.age = age;
        this.home = home;
        this.numbers = numbers;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", familyName='" + familyName + '\'' +
                ", age=" + age +
                ", home=" + home +
                ", numbers=" + numbers +
                ", someField='" + someField + '\'' +
                '}';
    }
}
