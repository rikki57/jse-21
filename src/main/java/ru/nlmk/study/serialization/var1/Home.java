package ru.nlmk.study.serialization.var1;

import java.io.Serializable;

public class Home implements Serializable {
    private String address;

    public Home(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Home{" +
                "address='" + address + '\'' +
                '}';
    }
}
