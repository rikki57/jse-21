package ru.nlmk.study.serialization.var2;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Login igor = new Login("Igor", "sfdsdfsdf");
        Login petr = new Login("Petr", "sdfkslfksdfs");
        System.out.println("igor = " + igor);
        System.out.println("petr = " + petr);

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("login.out"));
        objectOutputStream.writeObject(igor);
        objectOutputStream.writeObject(petr);
        objectOutputStream.close();

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("login.out"));
        Login igorRestored = (Login) objectInputStream.readObject();
        Login petrRestored = (Login) objectInputStream.readObject();

        System.out.println("igor = " + igorRestored);
        System.out.println("petr = " + petrRestored);
    }
}
