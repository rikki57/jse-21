package ru.nlmk.study.serialization.var3;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Igor", "Ivanov", new Address("Planernaya, 54-12", "Moscow", 101202),
                Arrays.asList("2-12-85-06", "7-15-65554"));

/*        Gson gson = new Gson();
        String personToJson = gson.toJson(person);
        System.out.println("personToJson = " + personToJson);
        Person personFromJson = gson.fromJson(personToJson, Person.class);
        System.out.println("personFromJson = " + personFromJson);*/

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        String json = "";
        String prettyJson = "";
        try {
            json = objectMapper.writeValueAsString(person);
            System.out.println("json = " + json);
            prettyJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(person);
            System.out.println("prettyJson = " + prettyJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            Person personFromJackson = objectMapper.readValue(prettyJson, Person.class);
            System.out.println("personFromJackson = " + personFromJackson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
