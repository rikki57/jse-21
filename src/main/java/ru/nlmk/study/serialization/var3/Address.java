package ru.nlmk.study.serialization.var3;

public class Address {
    private String streetAddress;
    private String city;
    private int postalCode;

    public Address() {
    }

    public Address(String streetAddress, String city, int postalCode) {
        this.streetAddress = streetAddress;
        this.city = city;
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "streetAddress='" + streetAddress + '\'' +
                ", city='" + city + '\'' +
                ", postalCode=" + postalCode +
                '}';
    }
}
