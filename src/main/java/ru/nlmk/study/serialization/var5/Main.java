package ru.nlmk.study.serialization.var5;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Igor", "Ivanov", new Address("Planernaya, 54-12", "Moscow", 101202),
                Arrays.asList("2-12-85-06", "7-15-65554"));

        XmlMapper xmlMapper = new XmlMapper();
        try {
            xmlMapper.writeValue(new File("jacksonExample.xml"), person);
        } catch (IOException e) {
            e.printStackTrace();
        }

        File file = new File("jacksonExample.xml");
        try {
            String xml = inputStreamToString(new FileInputStream(file));
            Person newPerson = xmlMapper.readValue(xml, Person.class);
            System.out.println("newPerson = " + newPerson);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
