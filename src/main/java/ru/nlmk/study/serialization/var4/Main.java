package ru.nlmk.study.serialization.var4;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class Main {
    public static void main(String[] args) {
/*        Person person = new Person("Igor", "Ivanov", new Address("Planernaya, 54-12", "Moscow", 101202),
                Arrays.asList("2-12-85-06", "7-15-65554"));*/

        File file = new File("jaxbExample.xml");
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ru.nlmk.study.serialization.var4.Person.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            //marshaller.marshal(person, file);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Person newPerson = (Person) unmarshaller.unmarshal(file);
            System.out.println("newPErson = " + newPerson);
        } catch (JAXBException e) {
            e.printStackTrace();
        }


    }
}
